package com.atlassian.ari.registry;

import com.atlassian.ari.ARI;
import com.atlassian.ari.ImmutableARI;
import org.jetbrains.annotations.NotNull;

public class HipChatARI {

    private static final String CONVERSATION_RESOURCE_TYPE = "conversation";
    private static final String PERMISSION_RESOURCE_TYPE = "permission";

    @NotNull
    public static ARI conversation(String cloudId, String conversationId) {
        return ImmutableARI.builder()
                .resourceOwner(ProductKey.HIPCHAT.getKey())
                .resourceType(CONVERSATION_RESOURCE_TYPE)
                .cloudId(cloudId)
                .resourceId(conversationId)
                .build();
    }

    @NotNull
    public static ARI permission(PermissionId permissionId) {
        return ImmutableARI.builder()
                .resourceOwner(ProductKey.HIPCHAT.getKey())
                .resourceType(PERMISSION_RESOURCE_TYPE)
                .resourceId(permissionId.getKey())
                .build();
    }

    public enum PermissionId {
        CHAT_ACCESS("chat/access"),

        CONVERSATION_ADD_USER("conversation/add-user"),
        CONVERSATION_REMOVE_USER("conversation/remove-user"),
        CONVERSATION_RENAME("conversation/rename"),
        CONVERSATION_SEND_MESSAGE("conversation/send-message"),
        CONVERSATION_TOGGLE_ARCHIVE("conversation/toggle-archive"),
        CONVERSATION_TOGGLE_PRIVACY("conversation/toggle-privacy"),
        CONVERSATION_VIEW("conversation/view");

        @NotNull
        private final String key;
        PermissionId(@NotNull final String key) {
            this.key = key;
        }

        @NotNull
        public String getKey() {
            return key;
        }
    }
}
