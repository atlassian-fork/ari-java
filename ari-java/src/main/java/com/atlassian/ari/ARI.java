package com.atlassian.ari;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import org.immutables.value.Value;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

@Value.Immutable
public abstract class ARI extends BaseARI {

    @JsonCreator
    @NotNull
    public static ARI parse(@NotNull String s) {
        return ARIParser.parse(s);
    }

    @NotNull
    public static Optional<ARI> tryParse(@NotNull String value) {
        return ARIParser.tryParse(value);
    }

    @Override
    @NotNull
    protected ARIPrefix getPrefix() {
        return ARIPrefix.CLOUD;
    }

    @JsonValue
    @Value.Lazy
    @Override
    @NotNull
    public String value() {
        return super.value();
    }

    /**
     * Added to enable use as javax.ws.PathParam
     * @param s the string to parse
     * @return the parsed ARI
     */
    @NotNull
    public static ARI valueOf(@NotNull String s) {
        return parse(s);
    }

}
