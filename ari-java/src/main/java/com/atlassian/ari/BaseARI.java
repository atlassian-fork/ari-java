package com.atlassian.ari;

import org.jetbrains.annotations.NotNull;

import java.util.Optional;

abstract class BaseARI {

    @NotNull
    public abstract String getResourceOwner();

    @NotNull
    public abstract Optional<String> getCloudId();

    @NotNull
    public abstract Optional<String> getResourceType();

    @NotNull
    public abstract Optional<String> getResourceId();

    @NotNull
    protected abstract ARIPrefix getPrefix();

    @NotNull
    public String value() {
        // Not using String.format for performance reason
        return getPrefix().value() + getResourceOwner() + ":" + getCloudId().orElse("") + ":"
                + getResourceType().orElse("") + "/" + getResourceId().orElse("");
    }


}
