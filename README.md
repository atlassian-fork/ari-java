# ARI Java

## Features

* Get ARI or filter from string
* Create ARI or filter from components

## Usage

```
<dependency>
    <groupId>com.atlassian.ari</groupId>
    <artifactId>ari-java</artifactId>
    <version>1.0</version>
</dependency>
```

## ARI
https://hello.atlassian.net/wiki/spaces/ARCH/pages/161909310/Atlassian+Resource+Identifier+Spec+draft-2.0 

### Get an ARI from a string

```java
// Throw an exception if the input is not valid
ARI ari = ARI.parse("ari:cloud:confluence:123:comment/1");

// Return an empty optional if the input is not valid
Optional<ARI> ari = ARI.tryParse("ari:cloud:confluence:123:comment/1");

```

### Create an ARI from the components
Cloud:

```java
ARI ari = ImmutableARI.builder()
                      .resourceOwner("confluence")
                      .cloudId("123")
                      .resourceType("comment")
                      .resourceId("1")
                      .build();
// ari.value() will return "ari:cloud:confluence:123:comment/1"
```

## ARI Filter
https://hello.atlassian.net/wiki/spaces/I/pages/164650817/Supported+ARI+Filters+in+the+API

### Get an ARI Filter from a string
```java
// Throw an exception if the input is not valid
ARIFilter ari = ARIFilter.parse("ari:filter:hipchat.cloud:*:conversation/*");

// Return an empty optional if the input is not valid
Optional<ARIFilter> ari = ARIFilter.tryParse("ari:filter:hipchat.cloud:*:conversation/*");

```

### Create an ARI Filter from the components
```java
ARIFilter ari = ImmutableARIFilter.builder()
                      .resourceOwner("hipchat.cloud")
                      .cloudId("*")
                      .resourceType("conversation")
                      .resourceId("*")
                      .build();
// ari.value() will return "ari:filter:hipchat.cloud:*:conversation/*"
```