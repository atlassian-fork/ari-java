package com.atlassian.ari;

import com.atlassian.ari.registry.HipChatARI;
import com.pholser.junit.quickcheck.generator.GenerationStatus;
import com.pholser.junit.quickcheck.generator.Generator;
import com.pholser.junit.quickcheck.random.SourceOfRandomness;

import java.util.UUID;

public class HipChatARIGenerator extends Generator<ARI> {
    public HipChatARIGenerator() {
        super(ARI.class);
    }

    @Override
    public ARI generate(SourceOfRandomness random, GenerationStatus status) {
        Generator<UUID> uuidGenerator = gen().type(UUID.class);
        String cloudId = uuidGenerator.generate(random, status).toString();
        String conversationId = uuidGenerator.generate(random, status).toString();

        return random.choose(new ARI[] {
                HipChatARI.conversation(cloudId, conversationId),
                HipChatARI.permission(random.choose(HipChatARI.PermissionId.values()))
        });
    }
}
